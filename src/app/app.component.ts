import { BookService } from './book.service';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'bookapp';
  searchValue;
  is = 0;
  startIndex = 0;
  maxResult = 8;
  constructor(private BookService:BookService,
    private route : ActivatedRoute, private router : Router) {}

  ngOnInit() {

  }
  change1(){
    this.is = 1;
  }
  getSearchValue(){
    //console.log(this.searchValue);
    this.is = 1;
    if(this.is == 1){
    let path = '/books';
    this.router.navigate([path],{queryParams: {searchText: this.searchValue,
      startIndex: this.startIndex, maxResults: this.maxResult}});
    }
  }

}
