import { Component, OnInit, Input } from '@angular/core';
import { BookService } from './../book.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-showbookimg',
  templateUrl: './showbookimg.component.html',
  styleUrls: ['./showbookimg.component.css']
})
export class ShowbookimgComponent implements OnInit {
  language = [{lang:"English", value1:'en'},{lang:"French",value1:'fr'},{lang:"Hindi", value1:'hi'}];
  filter = [{filter:"Full Volume",value1:'full'},{filter:"Free Google e-Books",value1:"free-ebooks"},{filter:"Paid Google e-books", value1:"paid-ebooks"}];
  search;
  is = 0;
  search1;
  url;
  data;
  getUrlValue = [];
  startIndex = 0;
  start;
  endpage;
  max;
  maxResult = 8;
  filterStructure=  null;
  filterSelected = "";
  languageStructure = null;
  languageSelected = "";
  langArray = null;
  filterArray = null;

  constructor(private BookService:BookService,
    private route : ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
      this.route.queryParamMap.subscribe(params => {
        this.is = 1;
        this.search = params.get("searchText");
        this.start = params.get("startIndex");
        this.endpage = params.get('maxResults');
        this.languageSelected = params.get("langRestrict");
        this.filterSelected = params.get("filter");

        if(this.languageSelected == null && this.filterSelected == null){
          this.languageSelected == "";
          this.filterSelected == "";
          this.url = "https://www.googleapis.com/books/v1/volumes?q="+this.search+"&startIndex="+this.startIndex+"&maxResults="+this.maxResult;
        }
        if(this.languageSelected !== null && this.filterSelected == null){
           this.url = "https://www.googleapis.com/books/v1/volumes?q="+this.search+"&startIndex="+this.startIndex+"&maxResults="+this.maxResult+"&langRestrict="+this.languageSelected;
        }
        if(this.languageSelected == null && this.filterSelected !== null){
          this.url = "https://www.googleapis.com/books/v1/volumes?q="+this.search+"&startIndex="+this.startIndex+"&maxResults="+this.maxResult+"&filter="+this.filterSelected;
        }
        if(this.languageSelected !== null && this.filterSelected !== null){
          this.url = "https://www.googleapis.com/books/v1/volumes?q="+this.search+"&startIndex="+this.startIndex+"&maxResults="+this.maxResult+"&langRestrict="+this.languageSelected+"&filter="+this.filterSelected;
       }
          //this.url = "https://www.googleapis.com/books/v1/volumes?q="+this.search+"&startIndex="+this.startIndex+"&maxResults="+this.maxResult;
        console.log(this.search);
        console.log(this.url);
        console.log(this.languageSelected);
        console.log(this.filterSelected);
        this.getnewData();
        this.getUpdate();
      });

  }

  getnewData(){
    this.startIndex = this.start ? +this.start : 0;
    this.maxResult = this.endpage ? +this.endpage : 8;
    this.BookService.getData(this.url)
    .subscribe(resp => {
      this.data = resp;
      this.max = this.data.totalItems;
      console.log(this.data);
      this.getUrlValue = this.data.items;
      console.log(this.getUrlValue);
      this.getUpdate();
    })
  }

  getUpdate(){
    this.languageStructure = {
      language: this.language,
      selected: this.languageSelected ? this.languageSelected : ""
      //selected: this.languageSelected
    };
    this.filterStructure = {
      filter: this.filter,
      selected: this.filterSelected ? this.filterSelected : ""
      //selected: this.filterSelected
    };
    console.log(this.languageSelected);
    console.log(this.filterSelected);
  }

  emitlanguage(){
    console.log(this.languageStructure);
    this.startIndex = 0;
    this.languageSelected = this.languageStructure.selected;
    console.log(this.languageSelected);
      if(this.languageSelected !== null && this.filterSelected == null){
        let path = '/books';
        this.router.navigate([path],{queryParams: {searchText: this.search, startIndex: this.startIndex,
         maxResults: this.maxResult, langRestrict: this.languageSelected}});
      }
      if(this.languageSelected !== null && this.filterSelected !== null){
        let path = '/books';
        this.router.navigate([path],{queryParams: {searchText: this.search, startIndex: this.startIndex,
         maxResults: this.maxResult, langRestrict: this.languageSelected, filter: this.filterSelected}});
      }
 }

  emitfilter(){
    console.log(this.filterStructure);
    this.startIndex = 0;
    console.log("langArray is empty");
    this.filterSelected = this.filterStructure.selected;
    console.log(this.filterSelected,this.languageSelected);
    if(this.languageSelected !== null && this.filterSelected !== null){
       let path = '/books';
       this.router.navigate([path],{queryParams: {searchText: this.search, startIndex: this.startIndex,
       maxResults: this.maxResult, langRestrict: this.languageSelected, filter: this.filterSelected}});
    }
    if(this.languageSelected == null && this.filterSelected !== null){
       let path = '/books';
       this.router.navigate([path],{queryParams: {searchText: this.search, startIndex: this.startIndex,
       maxResults: this.maxResult, filter: this.filterSelected}});
    }
  }

  getpage(x){
      this.startIndex = this.startIndex + x;
      console.log(this.startIndex);
      let path = '/books';
      this.router.navigate([path],{queryParams: {startIndex: this.startIndex,
      maxResults: this.maxResult, searchText: this.search}, queryParamsHandling: "merge"});
  }
}
