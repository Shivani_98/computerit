import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowbookimgComponent } from './showbookimg.component';

describe('ShowbookimgComponent', () => {
  let component: ShowbookimgComponent;
  let fixture: ComponentFixture<ShowbookimgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowbookimgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowbookimgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
